function(ultihashTools_move_to_project target)
  get_property(_output_name TARGET ${target} PROPERTY OUTPUT_NAME)
  get_filename_component(_output_name_exe "${_output_name}" NAME)
  file(RELATIVE_PATH _output_name
    ${CMAKE_CURRENT_BINARY_DIR} "${PROJECT_BINARY_DIR}/${target}")
  set_property(TARGET ${target}
    PROPERTY OUTPUT_NAME "${_output_name}")
endfunction()

# ------------------------------------------------------------------------------
function(ultihashTools_add_test test_name)
  include(CMakeParseArguments)

  set(_mat_flags
    HEADER_ONLY
    )

  set(_mat_one_variables
    TYPE
    MPI_NB_PROCS
    TARGET
    TEST_LIST
    )

  set(_mat_multi_variables
    SOURCES
    ARG_LIST
    LINK_LIBRARIES
    )

  cmake_parse_arguments(_mat_args
    "${_mat_flags}"
    "${_mat_one_variables}"
    "${_mat_multi_variables}"
    ${ARGN}
    )

  if ("${_mat_args_TYPE}" STREQUAL "BOOST")
  elseif("${_mat_args_TYPE}" STREQUAL "PYTHON")
  else ()
    message (SEND_ERROR "Can only handle types 'BOOST' and 'PYTHON'")
  endif ("${_mat_args_TYPE}" STREQUAL "BOOST")

  if ("${_mat_args_TYPE}" STREQUAL "BOOST")
    if(DEFINED _mat_args_TARGET)
      set(target_test_name ${_mat_args_TARGET})
    else()
      set(target_test_name ${test_name})
    endif()

    if(NOT TARGET ${target_test_name})
      add_executable(${target_test_name} ${_mat_args_SOURCES})
      if(_mat_args_TEST_LIST)
        set(_tmp ${${_mat_args_TEST_LIST}})
        list(APPEND _tmp ${target_test_name})
        set(${_mat_args_TEST_LIST} ${_tmp} PARENT_SCOPE)
      endif()

      ultihashTools_move_to_project(${target_test_name})
      target_link_libraries(${target_test_name}
        PRIVATE Boost::boost Boost::unit_test_framework cxxopts ${_mat_args_LINK_LIBRARIES})

      if(_mat_HEADER_ONLY)
        foreach(_target ${_mat_args_LINK_LIBRARIES})
          if(TARGET ${_target})
            get_target_property(_features ${_target} INTERFACE_COMPILE_FEATURES)
            target_compile_features(${target_test_name}
              PRIVATE ${_features})
          endif()
        endforeach()
      endif()
    endif()
  endif()

  if ("${_mat_args_TYPE}" STREQUAL "BOOST")
    set(_exe $<TARGET_FILE:${target_test_name}> ${_mat_args_UNPARSED_ARGUMENTS})
  else()
    set(_exe ${_mat_args_UNPARSED_ARGUMENTS} ${_mat_args_ARG_LIST})
  endif()

endfunction()
