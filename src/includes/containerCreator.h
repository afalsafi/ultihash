//
// Created by benjamin-elias on 19.06.21.
//
#include "conceptTypes.h"

#ifndef ULTIHASH_CONTAINERCREATOR_H
#define ULTIHASH_CONTAINERCREATOR_H

template <OptionalRecursiveContainer... T_values>
class containerCreator {
  using dataPack = std::variant<T_values...>;

 public:
  /* always returns whats inside DeepVariants or aiHeuristicVariant, extracts
   * the innermost currently held container this enables more complex number
   * systems to return a data snipped that can be encoded later and be written
   * to the Harddrive
   */
  template <
      typename T1,
      typename = typename std::enable_if<
          std::is_base_of<aiHeuristicVariant<T_values...>,
                          typename std::remove_reference<T1>::type>::value,
          T1>::type>
  inline auto pointerConcat(T1 && arg1) {
    return *std::forward<T1>(arg1);
  }

  /*
   * copying or appending Vectors can be optimized later on with GPU, AVX or
   * operating system
   */
  template <VectorStandard T1, VectorStandard T2>
  T1 vectorCopyArithmetic(T1 && arg1, T2 && arg2) {
    std::forward<T1>(arg1).reserve(std::forward<T1>(arg1).size() +
                                   std::forward<T2>(arg2).size());
    for (auto it = std::forward<T2>(arg2).begin();
         it != std::forward<T2>(arg2).end(); ++it) {
      std::forward<T1>(arg1).push_back(*it);
    }
    return std::forward<T1>(arg1);
  }

  /*
   * insert copy prefers to splice Lists onto each other but else we just use
   * the optimized insert method
   */
  template <SequentialContainer T1, SequentialContainer T2>
  T1 insertCopy(T1 && arg1, T2 && arg2) {
    if constexpr (List<T1, T2> and std::is_same_v<T1, T2>) {
      std::forward<T1>(arg1).splice(std::forward<T1>(arg1).end(), arg2);
    } else {
      std::forward<T1>(arg1).insert(std::forward<T1>(arg1).end(),
                                    std::forward<T2>(arg2).begin(),
                                    std::forward<T2>(arg2).end());
    }
    return std::forward<T1>(arg1);
  }

  /*
   * in the case the insert copy is used by a forward list we need to consider
   * that forward lists can only set elements to the front
   */
  template <ForwardList T1, SequentialContainer T2>
  T1 insertCopyForward(T1 && arg1, T2 && arg2) {
    using decayed = typename std::decay<T1>::type;
    if constexpr (std::is_same_v<T1, T2>) {
      std::forward<T2>(arg2).splice_after(std::forward<T2>(arg2).before_begin(),
                                          std::forward<T1>(arg1));
      return std::forward<T2>(arg2);
    } else {
      if constexpr (ForwardList<T2>) {
        decayed tmp0{};
        tmp0.assign(std::forward<T2>(arg2).begin(),
                    std::forward<T2>(arg2).end());
        return insertCopyForward(arg1, tmp0);
      } else {
        std::forward<T2>(arg2).insert(std::forward<T2>(arg2).begin(),
                                      std::forward<T1>(arg1).begin(),
                                      std::forward<T1>(arg1).end());
        decayed tmp{};
        for (auto i = std::forward<T2>(arg2).rbegin();
             i != std::forward<T2>(arg2).rend(); ++i) {
          tmp.push_front(*i);
        }
        return std::forward<T1>(arg1);
      }
    }
  }

 private:
  /*
   * this function returns maximum unsigned integer values, they can be used as
   * a mask
   */
  template <typename T1>
  inline const T1 maxVal() {
    if constexpr (std::is_same_v<T1, unsigned char>) {
      return UINT8_MAX;
    } else {
      if constexpr (std::is_same_v<T1, unsigned short>) {
        return UINT16_MAX;
      } else {
        if constexpr (std::is_same_v<T1, unsigned int>) {
          return UINT32_MAX;
        } else {
          if constexpr (std::is_same_v<T1, unsigned long int>) {
            return UINT64_MAX;
          }
        }
      }
    }
  }

 public:
  /*
   * If the containing types in both containers are different and an unsigned
   * type this function will cost the one onto the other. The function enables
   * us to optimize calculation later on as we can find out what commands
   * perform best using AVX. AVX can handle 4 times 16 bit or 2 times 32 bit or
   * with AVX2 8 times 16 bit or 4 times 32 bit or 2 times 64 bit to calculate
   * simultaneously. All operation combinations have different speeds that we
   * need to find the fastest of so that we can internally calculate fast over a
   * byte arry laying under our chosen container structure C++ is little endian
   * oriented and will convert automatically
   */
  template <SequentialContainer T1, SequentialContainer T2>
  T1 unsignedIntegralConvertCopy(T1 && arg1, T2 && arg2) {
    /* get how many bytes are used for each internal element, this will let us
     * know how many fundamental types we will need to represent the other type
     * or how many bytes fit and so the second fundamental type may fit into the
     * first type
     */
    const short first =
        sizeof(boost::mp11::mp_first<typename std::decay<T1>::type>);
    const short second =
        sizeof(boost::mp11::mp_first<typename std::decay<T2>::type>);
    if constexpr (first > second) {
      // matching more than two fundamental types of arg2 into arg1
      const short mult = first / second;
      boost::mp11::mp_first<typename std::decay<T2>::type> buf[mult];
      for (short i = 0; i < mult; i++) {
        buf[i] = 0;
      }
      short count = 0;
      boost::mp11::mp_first<typename std::decay<T1>::type> out = 0;
      for (auto & i : std::forward<T2>(arg2)) {
        buf[count] = i;
        ++count;
        if (count >= mult) {
          out = reinterpret_cast<
              boost::mp11::mp_first<typename std::decay<T1>::type> *>(buf)[0];
          for (short i3 = 0; i3 < mult; i3++) {
            buf[i3] = 0;
          }
          std::forward<T1>(arg1).push_back(out);
          count = 0;
        }
      }
      out = reinterpret_cast<
          boost::mp11::mp_first<typename std::decay<T2>::type> *>(buf)[0];
      for (short i3 = 0; i3 < mult; i3++) {
        buf[i3] = 0;
      }
      if (out > 0)
        std::forward<T1>(arg1).push_back(out);
    } else {
      // generating enough fundamental types of type arg1 to represent arg2
      const short mult = second / first;
      for (auto & i : std::forward<T2>(arg2)) {
        boost::mp11::mp_first<typename std::decay<T1>::type> buf[mult];
        boost::mp11::mp_first<typename std::decay<T2>::type> mask =
            maxVal<boost::mp11::mp_first<typename std::decay<T1>::type>>();
        boost::mp11::mp_first<typename std::decay<T2>::type> tmp;
        for (short i2 = 0; i2 < mult; ++i2) {
          tmp = i & mask;
          tmp >>= 8 *
                  sizeof(boost::mp11::mp_first<typename std::decay<T1>::type>) *
                  i2;
          buf[i2] =
              static_cast<boost::mp11::mp_first<typename std::decay<T1>::type>>(
                  tmp);
          mask <<=
              8 * sizeof(boost::mp11::mp_first<typename std::decay<T1>::type>);
        }
        for (short i2 = 0; i2 < mult; ++i2) {
          std::forward<T1>(arg1).push_back(buf[i2]);
        }
      }
    }
    return std::forward<T1>(arg1);
  }

  /*
   * to handle a ForwardList at the front we need to do the conversion from the
   * function above in reverse in this case we need to mention that if the first
   * inner type is bigger than the second one, we need to swap order of the
   * second type due to reading reverse and appending to the front
   */
  template <ForwardList T1, SequentialContainer T2>
  T1 unsignedIntegralConvertForwardCopy(T1 && arg1, T2 && arg2) {
    using decayed = typename std::decay<T1>::type;
    if constexpr (ForwardList<typename std::decay<T2>::type>) {
      std::vector<boost::mp11::mp_front<typename std::decay<T2>::type>> tmp{};
      tmp.assign(std::forward<T2>(arg2).begin(), std::forward<T2>(arg2).end());
      return unsignedIntegralConvertForwardCopy(arg1, tmp);
    } else {
      const short first =
          sizeof(boost::mp11::mp_first<typename std::decay<T1>::type>);
      const short second =
          sizeof(boost::mp11::mp_first<typename std::decay<T2>::type>);
      if constexpr (first > second) {
        // matching more than two fundamental types of arg2 into arg1
        const short mult = first / second;
        boost::mp11::mp_first<typename std::decay<T2>::type> buf[mult];
        for (short i = 0; i < mult; i++) {
          buf[i] = 0;
        }
        short count = 0;
        boost::mp11::mp_first<typename std::decay<T1>::type> out = 0;
        decayed tmp{};
        for (auto i = std::forward<T2>(arg2).rbegin();
             i != std::forward<T2>(arg2).rend(); --i) {
          buf[mult - 1 - count] = *i;
          ++count;
          if (count >= mult) {
            out = reinterpret_cast<
                boost::mp11::mp_first<typename std::decay<T1>::type> *>(buf)[0];
            for (short i3 = 0; i3 < mult; i3++) {
              buf[i3] = 0;
            }
            tmp.push_front(out);
            count = 0;
          }
        }
        out = reinterpret_cast<
            boost::mp11::mp_first<typename std::decay<T2>::type> *>(buf)[0];
        if (out > 0)
          tmp.push_front(out);
        tmp.splice_after(tmp.before_begin(), std::forward<T1>(arg1));
        return tmp;
      } else {
        // generating enough fundamental types of type arg1 to represent arg2
        const short mult = second / first;
        for (auto i = std::forward<T2>(arg2).rbegin();
             i != std::forward<T2>(arg2).rend(); --i) {
          boost::mp11::mp_first<typename std::decay<T1>::type> buf[mult];
          boost::mp11::mp_first<typename std::decay<T2>::type> mask =
              maxVal<boost::mp11::mp_first<typename std::decay<T1>::type>>();
          boost::mp11::mp_first<typename std::decay<T2>::type> tmp;
          for (short i2 = 0; i2 < mult; ++i2) {
            tmp = *i & mask;
            tmp >>=
                8 *
                sizeof(boost::mp11::mp_first<typename std::decay<T1>::type>) *
                i2;
            buf[i2] = static_cast<
                boost::mp11::mp_first<typename std::decay<T1>::type>>(tmp);
            mask <<=
                8 *
                sizeof(boost::mp11::mp_first<typename std::decay<T1>::type>);
          }
          for (short i2 = mult - 1; i2 >= 0; --i2) {
            std::forward<T1>(arg1).push_front(buf[i2]);
          }
        }
        return std::forward<T1>(arg1);
      }
    }
  }

  /*
   * anything else will copy and convert all elements from container a to
   * container b
   */
  template <class T1, class T2>
  auto restCopy(T1 && arg1, T2 && arg2) {
    if constexpr (boost::mp11::mp_contains<
                      dataPack, typename std::decay<T1>::type>::value) {
      for (auto & i : std::forward<T2>(arg2)) {
        auto tmp = createSingle<typename std::decay<T1>::type>(i);
        if constexpr (DeepVariantContainerDecay<decltype(tmp)>) {
          boost::mp11::mp_with_index<boost::mp11::mp_size<decltype(tmp)>>(
              tmp.index(), [&](auto I) {
                arg1 = argConcatContainer(arg1, std::get<I>(tmp));
              });
        } else {
          arg1 = argConcatContainer(arg1, tmp);
        }
      }
      return std::forward<T1>(arg1);
    } else {
      if constexpr (boost::mp11::mp_contains<
                        dataPack, typename std::decay<T2>::type>::value) {
        typename std::decay<T2>::type in =
            argConcatContainer(typename std::decay<T2>::type{}, arg1);
        for (auto & i : std::forward<T2>(arg2)) {
          auto tmp = createSingle<typename std::decay<T1>::type>(i);
          if constexpr (DeepVariantContainerDecay<decltype(tmp)>) {
            boost::mp11::mp_with_index<boost::mp11::mp_size<decltype(tmp)>>(
                tmp.index(),
                [&](auto I) { in = argConcatContainer(in, std::get<I>(tmp)); });
          } else {
            in = argConcatContainer(in, tmp);
          }
        }
        return in;
      } else {
        return argConcatContainer(
            argConcatContainer(boost::mp11::mp_first<dataPack>{}, arg1), arg2);
      }
    }
  }

  /*
   * restForwardCopy is most complicated because we cannot say for sure anything
   * about the internal types. For that reason we have to convert the first
   * ForwardList and then standard restCopy the second container, where we even
   * don't know if that second container is derived from aiHeuristicVariant
   */
  template <ForwardList T1, class T2>
  auto restForwardCopy(T1 && arg1, T2 && arg2) {
    if constexpr (std::is_same_v<T1, T2>) {
      std::forward<T2>(arg2).splice_after(std::forward<T2>(arg2).before_begin(),
                                          arg1);
      return std::forward<T2>(arg2);
    } else {
      std::vector<boost::mp11::mp_first<typename std::decay<T1>::type>> tmp;
      for (auto & i : std::forward<T1>(arg1)) {
        tmp.push_back(i);
      }
      auto tmp2 = restCopy(tmp, arg2);
      std::forward<T1>(arg1).assign(tmp2.begin(), tmp2.end());
      return std::forward<T1>(arg1);
    }
  }

  /*
   * argConcatContainer can concat two sequential containers of any type
   * including strings after certain rules: if the two container types are both
   * one of the following concepts they will be processed as a special filter
   * rule in this function the first argument is set as reference point for the
   * output type, at return the type of arg2 has to be converted into the type
   * of arg1
   */
  template <SequentialContainerExtendedDecay T1,
            SequentialContainerExtendedDecay T2>
  inline auto argConcatContainer(T1 && arg1, T2 && arg2) {
    // one empty and first equal container types will cause no action but only
    // forwarding
    if constexpr (std::is_same_v<typename std::decay<T1>::type,
                                 typename std::decay<T2>::type> and
                  !ForwardList<T1>) {
      if (std::forward<T1>(arg1).size() == 0) {
        return std::forward<T2>(arg2);
      }
    }
    if constexpr (std::is_same_v<typename std::decay<T1>::type,
                                 typename std::decay<T2>::type> and
                  ForwardList<T1>) {
      if (std::forward<T1>(arg1).begin() == std::forward<T1>(arg1).end()) {
        return std::forward<T2>(arg2);
      }
    }
    // main block
    // plain vector copy that can be optimized with GPU, AVX or Operating System
    if constexpr (VectorCopyArithmeticDecay<T1, T2>) {
      return vectorCopyArithmetic(arg1, arg2);
    } else {
      /* structures that support insert, because insert is optimized over the
       * std library for each container both types must be within the concept
       */
      if constexpr (InsertCopyDecay<T1, T2>) {
        return insertCopy(arg1, arg2);
      } else {
        /* ForwardList structures that support insert, because insert is
         * optimized over the std library for each container both types must be
         * within the concept
         */
        if constexpr (InsertCopyForwardDecay<T1, T2>) {
          return insertCopyForward(arg1, arg2);
        } else {
          /*
           * This function will transform unsigned integer numbers and merge or
           * split them to castings that are defined in the first argument
           */
          if constexpr (UnsignedIntegralConvertCopyDecay<T1, T2>) {
            return unsignedIntegralConvertCopy(arg1, arg2);
          } else {
            if constexpr (UnsignedIntegralConvertCopyForwardDecay<T1, T2>) {
              return unsignedIntegralConvertForwardCopy(arg1, arg2);
            } else {
              /*
               * concatenating strings
               */
              if constexpr (String<T1, T2>) {
                return std::forward<T1>(arg1).append(std::forward<T2>(arg2));
              } else {
                /*
                 * this module will just do a sequential CPU copy from one
                 * container to another, no matter what the container types are.
                 * While copying a conversion can be applied
                 */
                if constexpr (!ForwardList<T1>) {
                  return restCopy(arg1, arg2);
                } else {
                  return restForwardCopy(arg1, arg2);
                }
              }
            }
          }
        }
      }
    }
  }

  /*
   * argConcat connects two objects and always returns a sequential container.
   * It unpacks objects derived from aiHeuristic variant to a container and
   * packs fundamental types or classes into a container under possible
   * conversion.
   * In the 1st case we only have Arithmetics we need to wrap both into the
   * inner container type of dataPack. In the 2nd case when we have a container
   * type or aiHeuristics derived object we wrap the fundamental type into that
   * Note: non arithmetics should be forwarded while copying fundamental types
   * is faster than forwarding due to the address lookup overhead that is
   * created by forwarding. So forwarding it only legit for big complex objects
   * on heap
   */
  template <Arithmetic T1, Arithmetic T2>
  auto argConcat(T1 arg1, T2 arg2) {
    return argConcat(create(arg1), create(arg2));
  }

  template <Arithmetic T1, NoArithmetic T2>
  auto argConcat(T1 arg1, T2 && arg2) {
    if constexpr ((BigNumberConcept<T2> and !String<T2>) or
                  std::is_base_of<
                      aiHeuristicVariant<T_values...>,
                      typename std::remove_reference<T2>::type>::value) {
      return argConcat(arg1, pointerConcat(arg2));
    } else {
      if constexpr (DeepVariantContainerDecay<T2>) {
        using containerType2 =
            typename boost::mp11::mp_front<typename std::decay<T2>::type>;
        return argConcat(createSingle<containerType2>(arg1), arg2);
      } else {
        if constexpr (SequentialContainer<T2>) {
          using containerType2 = typename std::decay<T2>::type;
          return argConcat(createSingle<containerType2>(arg1), arg2);
        } else {
          return argConcat(arg1, create(arg2));
        }
      }
    }
  }

  template <NoArithmetic T1, Arithmetic T2>
  auto argConcat(T1 && arg1, T2 arg2) {
    if constexpr ((BigNumberConcept<T1> and !String<T1>) or
                  std::is_base_of<
                      aiHeuristicVariant<T_values...>,
                      typename std::remove_reference<T1>::type>::value) {
      return argConcat(pointerConcat(arg1), arg2);
    } else {
      if constexpr (DeepVariantContainerDecay<T1>) {
        using containerType1 =
            typename boost::mp11::mp_front<typename std::decay<T1>::type>;
        return argConcat(arg1, createSingle<containerType1>(arg2));
      } else {
        if constexpr (SequentialContainer<T1>) {
          using containerType1 = typename std::decay<T1>::type;
          return argConcat(arg1, createSingle<containerType1>(arg2));
        } else {
          return argConcat(create(arg1), arg2);
        }
      }
    }
  }

  /*
   * argConcant main function can concat exactly two combined or sequential
   * container types into each other the rules are simple: if one internal type
   * matches the inner dataPack type, that type will be output type, but the
   * used container will be output container if there are two different internal
   * types, that also differ the inner dataPack type, they will be converted
   * into the inner dataPack type and the container type will be chosen as the
   * first wrapping container type used as input type
   */
  template <NoArithmetic T1, NoArithmetic T2>
  auto argConcat(T1 && arg1, T2 && arg2) {
    if constexpr (((BigNumberConcept<T1> and !String<T1>) or
                   std::is_base_of<
                       aiHeuristicVariant<T_values...>,
                       typename std::remove_reference<T1>::type>::value) and
                  ((BigNumberConcept<T2> and !String<T2>) or
                   std::is_base_of<
                       aiHeuristicVariant<T_values...>,
                       typename std::remove_reference<T2>::type>::value)) {
      return argConcat(pointerConcat(arg1), pointerConcat(arg2));
    } else {
      if constexpr ((BigNumberConcept<T1> and !String<T1>) or
                    std::is_base_of<
                        aiHeuristicVariant<T_values...>,
                        typename std::remove_reference<T1>::type>::value) {
        return argConcat(pointerConcat(arg1), arg2);
      } else {
        if constexpr ((BigNumberConcept<T2> and !String<T2>) or
                      std::is_base_of<
                          aiHeuristicVariant<T_values...>,
                          typename std::remove_reference<T2>::type>::value) {
          return argConcat(arg1, pointerConcat(arg2));
        } else {
          using type1out = typename std::decay<T1>::type;
          using type2out = typename std::decay<T2>::type;
          if constexpr (!(std::is_same_v<type1out, dataPack> or
                          boost::mp11::mp_contains<dataPack, type1out>::value or
                          OptionalRecursiveContainerDecay<
                              T1>)and!(std::is_same_v<type2out, dataPack> or
                                       boost::mp11::mp_contains<
                                           dataPack, type2out>::value or
                                       OptionalRecursiveContainerDecay<T2>)) {
            return argConcat(create(arg1), create(arg2));
          } else {
            if constexpr (!(std::is_same_v<type1out, dataPack> or
                            boost::mp11::mp_contains<dataPack,
                                                     type1out>::value or
                            OptionalRecursiveContainerDecay<
                                T1>)and(std::is_same_v<type2out, dataPack> or
                                        boost::mp11::mp_contains<
                                            dataPack, type2out>::value or
                                        OptionalRecursiveContainerDecay<T2>)) {
              if constexpr (DeepVariantContainerDecay<T2>) {
                using containerType2 = typename boost::mp11::mp_front<
                    typename std::decay<T2>::type>;
                return argConcat(createSingle<containerType2>(arg1), arg2);
              } else {
                if constexpr (SequentialContainer<T2>) {
                  using containerType2 = typename std::decay<T2>::type;
                  return argConcat(createSingle<containerType2>(arg1), arg2);
                } else {
                  return argConcat(arg1, create(arg2));
                }
              }
            } else {
              if constexpr ((std::is_same_v<type1out, dataPack> or
                             boost::mp11::mp_contains<dataPack,
                                                      type1out>::value or
                             OptionalRecursiveContainerDecay<
                                 T1>)and!(std::is_same_v<type2out, dataPack> or
                                          boost::mp11::mp_contains<
                                              dataPack, type2out>::value or
                                          OptionalRecursiveContainerDecay<
                                              T2>)) {
                if constexpr (DeepVariantContainerDecay<T1>) {
                  using containerType1 = typename boost::mp11::mp_front<
                      typename std::decay<T1>::type>;
                  return argConcat(arg1, createSingle<containerType1>(arg2));
                } else {
                  if constexpr (SequentialContainer<T1>) {
                    using containerType1 = typename std::decay<T1>::type;
                    return argConcat(arg1, createSingle<containerType1>(arg2));
                  } else {
                    return argConcat(create(arg1), arg2);
                  }
                }
              } else {
                dataPack out;
                if constexpr (DeepVariantContainerDecay<T1>) {
                  boost::mp11::mp_with_index<
                      boost::mp11::mp_size<typename std::decay<T1>::type>>(
                      std::forward<T1>(arg1).index(), [&](auto I) {
                        out = argConcat(std::get<I>(std::forward<T1>(arg1)),
                                        arg2);
                      });
                } else {
                  if constexpr (DeepVariantContainerDecay<T2>) {
                    boost::mp11::mp_with_index<
                        boost::mp11::mp_size<typename std::decay<T2>::type>>(
                        std::forward<T2>(arg2).index(), [&](auto I) {
                          out = argConcat(arg1,
                                          std::get<I>(std::forward<T2>(arg2)));
                        });
                  } else {
                    if constexpr (boost::mp11::mp_contains<
                                      dataPack,
                                      typename std::decay<T1>::type>::value) {
                      out = argConcatContainer(arg1, arg2);
                    } else {
                      if constexpr (boost::mp11::mp_contains<
                                        dataPack,
                                        typename std::decay<T2>::type>::value) {
                        out = argConcat(typename std::decay<T2>::type{},
                                        argConcatContainer(arg1, arg2));
                      } else {
                        out = argConcat(boost::mp11::mp_first<dataPack>{},
                                        argConcatContainer(arg1, arg2));
                      }
                    }
                  }
                }
                return out;
              }
            }
          }
        }
      }
    }
  }

  /*
   * an empty create function just creates a standard container type, that means
   * simply the first container type given in dataPack
   */
  auto create() { return boost::mp11::mp_front<dataPack>{}; }

  /*
   * Since the container creator can reference to the dataPack what internal
   * type should have higher priority, we are now able to convert a create with
   * only one argument into the internal given type, since there is no second
   * container type available that we could reference to
   */
  template <typename containerType = boost::mp11::mp_front<dataPack>,
            Arithmetic T1>
  auto createSingle(T1 arg1) {
    using innerType = boost::mp11::mp_first<containerType>;
    if constexpr (Arithmetic<innerType>) {
      if constexpr (InnerContainerTypeOfDecay<T1, containerType>) {
        return containerType{arg1};
      } else {
        if constexpr (!FloatingPoint<innerType>) {
          if constexpr (UnsignedIntegral<innerType>) {
            return containerType{static_cast<innerType>(round(abs(arg1)))};
          } else {
            return containerType{static_cast<innerType>(round(arg1))};
          }
        } else {
          if constexpr (UnsignedIntegral<innerType>) {
            return containerType{static_cast<innerType>(abs(arg1))};
          } else {
            return containerType{static_cast<innerType>(arg1)};
          }
        }
      }
    } else {
      if constexpr (String<innerType>) {
        return containerType{std::to_string(arg1)};
      } else {
        return containerType{innerType{arg1}};
      }
    }
  }

  template <typename containerType = boost::mp11::mp_front<dataPack>,
            NoArithmetic T1>
  auto createSingle(T1 && arg1) {
    if constexpr ((BigNumberConcept<T1> and !String<T1>) or
                  std::is_base_of<
                      aiHeuristicVariant<T_values...>,
                      typename std::remove_reference<T1>::type>::value) {
      return createSingle(pointerConcat(arg1));
    } else {
      if constexpr (OptionalRecursiveContainerDecay<T1>) {
        return std::forward<T1>(arg1);
      } else {
        using innerType = boost::mp11::mp_first<containerType>;
        if constexpr (Arithmetic<innerType> and String<T1>) {
          if constexpr (std::is_same_v<innerType, long double>) {
            return containerType{std::stold(std::forward<T1>(arg1))};
          }
          if constexpr (std::is_same_v<innerType, double>) {
            return containerType{std::stod(std::forward<T1>(arg1))};
          }
          if constexpr (std::is_same_v<innerType, float>) {
            return containerType{std::stof(std::forward<T1>(arg1))};
          }
          if constexpr (std::is_same_v<innerType, char>) {
            return containerType{
                static_cast<char>(std::stoi(std::forward<T1>(arg1)))};
          }
          if constexpr (std::is_same_v<innerType, short>) {
            return containerType{
                static_cast<short>(std::stoi(std::forward<T1>(arg1)))};
          }
          if constexpr (std::is_same_v<innerType, int>) {
            return containerType{std::stoi(std::forward<T1>(arg1))};
          }
          if constexpr (std::is_same_v<innerType, long>) {
            return containerType{std::stol(std::forward<T1>(arg1))};
          }
          if constexpr (std::is_same_v<innerType, long long>) {
            return containerType{std::stoll(std::forward<T1>(arg1))};
          }
          if constexpr (std::is_same_v<innerType, unsigned char>) {
            return containerType{
                static_cast<unsigned char>(std::stoul(std::forward<T1>(arg1)))};
          }
          if constexpr (std::is_same_v<innerType, unsigned short>) {
            return containerType{static_cast<unsigned short>(
                std::stoul(std::forward<T1>(arg1)))};
          }
          if constexpr (std::is_same_v<innerType, unsigned int>) {
            return containerType{std::stoul(std::forward<T1>(arg1))};
          }
          if constexpr (std::is_same_v<innerType, unsigned long>) {
            return containerType{std::stoul(std::forward<T1>(arg1))};
          }
          if constexpr (std::is_same_v<innerType, unsigned long long>) {
            return containerType{std::stoull(std::forward<T1>(arg1))};
          }
        } else {
          if constexpr (std::is_base_of_v<innerType,
                                          typename std::decay<T1>::type>) {
            return containerType{
                dynamic_cast<innerType &>(std::forward<T1>(arg1))};
          } else {
            if constexpr (InnerContainerTypeOfDecay<T1, containerType> or
                          boost::mp11::mp_similar<
                              T1, std::initializer_list<innerType>>::value) {
              return containerType{std::forward<T1>(arg1)};
            } else {
              return containerType{innerType{std::forward<T1>(arg1)}};
            }
          }
        }
      }
    }
  }

  template <Arithmetic T1>
  auto create(T1 arg1) {
    return createSingle(arg1);
  }

  template <NoArithmetic T1>
  auto create(T1 && arg1) {
    return createSingle(arg1);
  }

  /*
   * the createRecursive function forwards over all input arguments and
   * recursively concatenates the input types. it is mostly called by create
   */
  template <Arithmetic T1, Arithmetic T2, typename... Args>
  auto createRecursive(T1 arg1, T2 arg2, Args &&... args) {
    if constexpr (sizeof...(args) == 0) {
      return argConcat(arg1, arg2);
    } else {
      auto tmp = argConcat(arg1, arg2);
      return createRecursive(tmp, args...);
    }
  }

  template <Arithmetic T1, NoArithmetic T2, typename... Args>
  auto createRecursive(T1 arg1, T2 && arg2, Args &&... args) {
    if constexpr (sizeof...(args) == 0) {
      return argConcat(arg1, arg2);
    } else {
      auto tmp = argConcat(arg1, arg2);
      return createRecursive(tmp, args...);
    }
  }

  template <NoArithmetic T1, Arithmetic T2, typename... Args>
  auto createRecursive(T1 && arg1, T2 arg2, Args &&... args) {
    if constexpr (sizeof...(args) == 0) {
      return argConcat(arg1, arg2);
    } else {
      auto tmp = argConcat(arg1, arg2);
      return createRecursive(tmp, args...);
    }
  }

  template <NoArithmetic T1, NoArithmetic T2, typename... Args>
  auto createRecursive(T1 && arg1, T2 && arg2, Args &&... args) {
    if constexpr (IteratorType<T1, T2>) {
      auto beginIt = std::forward<T1>(arg1);
      auto endIt = std::forward<T2>(arg2);
      std::vector<typename std::decay<decltype(*beginIt)>::type> buffer{};
      while (beginIt != endIt) {
        buffer.push_back(*beginIt);
        beginIt++;
      }
      return create(buffer, args...);
      // return fastForwardEnd(tmp,arg1,arg2,args...);
    } else {
      if constexpr (!IteratorType<T1> and IteratorType<T2>) {
        return createRecursive(arg1, createRecursive(arg2, args...));
        // return fastForwardEnd(arg1,arg2, args...);
      } else {
        if constexpr (sizeof...(args) == 0) {
          return argConcat(arg1, arg2);
        } else {
          auto tmp = argConcat(arg1, arg2);
          return createRecursive(tmp, args...);
        }
      }
    }
  }

  /*
   * the create function concatenates any input types coming in.
   * if one of the first two standing types is not an aiHeuristic derived
   * object, some std::variant with containers in it or a sequential container,
   * the function will try to merge that fundamental type into the existing
   * container doing anything necessary to convert. That means you go at risk of
   * rounding and converting data into each other. The rules can be read in the
   * conceptTypes section
   */
  template <typename T1, Arithmetic T2, typename... Args>
  auto create(T1 arg1, T2 arg2, Args &&... args) {
    return createRecursive(arg1, arg2, args...);
  }

  template <typename T1, NoArithmeticIterator T2, typename... Args>
  auto create(T1 arg1, T2 && arg2, Args &&... args) {
    return createRecursive(arg1, arg2, args...);
  }

  template <typename T1, IteratorType T2, typename... Args>
  auto create(T1 arg1, T2 arg2, Args &&... args) {
    return createRecursive(arg1, arg2, args...);
  }

  /*
   * function capable of taking the first argument as output type and converting
   * all following arguments into that type input types can be any sequential
   * containers, std::variants or classes that are derived from aiHeristics, so
   * they contain an std::variant that can be extracted to be merged into the
   * output type the container cast first uses the create function to merge
   * everythig into one single object and then it will convert to output type
   */
  template <OptionalRecursiveContainer ToType, typename FromType,
            typename... Args>
  ToType container_cast(FromType && source, Args &&... args) {
    ToType tmp{};
    if constexpr (DeepVariantContainerDecay<ToType>) {
      return create(tmp, source, args...);
    } else {
      auto convert = create(tmp, source, args...);
      boost::mp11::mp_with_index<boost::mp11::mp_size<decltype(convert)>>(
          std::forward<decltype(convert)>(convert).index(), [&](auto I) {
            auto tmp_conv = std::get<I>(convert);
            tmp = argConcatContainer(tmp, tmp_conv);
          });
    }

    return tmp;
  }

  containerCreator() = default;
};

#endif  // ULTIHASH_CONTAINERCREATOR_H
