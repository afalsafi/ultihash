#include <iostream>
#include <variant>
#include <string>
#include <cassert>

#include "includes/conceptTypes.h"
#include "includes/containerCreator.h"

int main() {
  using R_devec_t = boost::container::devector<double>;
  using I_devec_t = boost::container::devector<int>;
  // using B_devec_t = boost::container::devector<bool>;

  using R_vec_t = boost::container::vector<double>;
  using I_vec_t = boost::container::vector<int>;
  using B_vec_t = boost::container::vector<bool>;

  R_devec_t nums_devec_real({9.34632, 2.126, 8.468123, 1.7384});
  R_devec_t nums_devec_real2({9.34632, 2.126, 8.468123, 1.7384});
  I_devec_t nums_devec_int({4, 5, 6, 7});

  R_vec_t nums_vec_real({342, 46536, 1342.234});
  R_vec_t nums_vec_real2({22, -3424.2423, 0.91347});
  I_vec_t nums_vec_int({342, 46536, 1342});
  B_vec_t bools_vec({true, true, false, true, false});

  double num_real{1.7};
  int num_int{23};
  char num_char{'b'};

  containerCreator<R_devec_t> my_creator;

  auto && my_container_1 = my_creator.createSingle(num_real);

  std::cout << "\n create single"
            << "\n";

  for (auto && val : my_container_1) {
    std::cout << "val: " << val << "\n";
  }

  std::cout << "\n create recursive"
            << "\n";
  auto && container_mix{
      my_creator.createRecursive(num_real, num_int, nums_devec_real, num_char,
                                 nums_devec_int, nums_vec_real, nums_vec_int)};

  auto && container_vals{std::get<0>(container_mix)};

  for (auto && val : container_vals) {
    std::cout << "value is: " << val << "\n";
  }

  auto && copied_container{
      my_creator.insertCopy(nums_vec_real, nums_vec_real2)};

  std::cout << "\n Insert Copy"
            << "\n";

  for (auto && val : copied_container) {
    std::cout << "nums_vec_real value is: " << val << "\n";
  }

  auto && concat_container{
      my_creator.argConcat(container_mix, copied_container)};

  std::cout << "\n Argument Concat"
            << "\n";

  for (auto && val : std::get<0>(concat_container)) {
    std::cout << "nums_vec_real value is: " << val << "\n";
  }

  // auto && copied_forward_container{
  //     my_creator.insertCopyForward(nums_devec_real, nums_vec_real2)};

  std::cout << "\n Convert Copy"
            << "\n";


  auto && converted_num_bool{
    my_creator.unsignedIntegralConvertCopy(nums_devec_real, bools_vec)};

  for (auto && val : converted_num_bool) {
    std::cout << "converted value is: " << val << "\n";
  }


  // auto && converted_bool_num{
  //     my_creator.unsignedIntegralConvertCopy(bools_vec, nums_devec_real)};

  // for (auto && val : converted_bool_num) {
  //   std::cout << "converted value is: " << val << "\n";
  // }

  return 0;
}
